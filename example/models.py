from django.db import models

# Create your models here.

class Author(models.Model):
    name = models.CharField(max_length=64)

    def __unicode__(self):
        return u'%s' % self.name

class Book(models.Model):
    title = models.CharField(max_length=64)
    author = models.ForeignKey(Author)

    def __unicode__(self):
        return u'%s' % self.title
