from django.contrib import admin
from .models import Author, Book


def book_author_name(obj):
    return obj.author.name
book_author_name.admin_order_field = 'author__name'


class BookAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        book_author_name,
    )


admin.site.register(Author)
admin.site.register(Book, BookAdmin)

